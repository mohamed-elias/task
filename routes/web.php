<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('id', '[0-9]+');
Route::pattern('status', '[0-9]');
Route::pattern('order', '[0-9:_]+');

Route::get('/', 'FrontController@index');
Route::get('/home', 'FrontController@index');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware('auth')->group(function () {
	Route::prefix('admin')->group(function () {
		Route::get('/', 'HomeController@index');

		Route::prefix('posts')->group(function () {
			Route::get('/', 'PostsController@index');
			Route::get('add', 'PostsController@create');
			Route::post('add', 'PostsController@store');
			Route::get('status/{id}/{status}', 'PostsController@status');
			Route::get('order/{order}', 'PostsController@order');
			Route::get('{string}',  function () {
				return redirect('posts/404', 404);
			});
			Route::get('404',  function () {
				return view('admin/404');
			});
		});
	});
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
