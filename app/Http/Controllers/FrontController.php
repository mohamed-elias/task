<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $feat_posts = Post::where(array('status'=>1))->orderBy('order','ASC')->get();
      $none_posts = Post::where(array('status'=>0))->orderBy('order','ASC')->get();
      return view('web/index' , array('feat_posts'=>$feat_posts,'none_posts'=>$none_posts));
    }
}
