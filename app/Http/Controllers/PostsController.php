<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Post;
use App\User;
use Auth;

class PostsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->middleware('auth');
		$this->middleware(function ($request, $next) {
			$this->user = Auth::user();
			if(!$this->user->id) return redirect('web/404');
			else 
			{
				return $next($request);
			}
		});
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = Post::orderBy('order','ASC')->paginate(10);
	  $max = Post::max('order');
      return view('admin/show' , array('alldata'=>$data,'max'=>$max));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$types = Type::all();
        return view('admin/add', array('types'=>$types));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$attrs = array(
			'title' => 'Title',
			'description' => 'Description',
			'type' => 'Type',
			'image' => 'Image',
		);
		$this->validate(request(),array(
			'title' => 'required|max:191',
			'description' => 'required',
			'type' => 'required|max:191',
			'image' => 'mimes:jpeg|dimensions:width=360,height=240',
			//'image' => 'mimes:jpeg',
		), array(), $attrs);

		
		$maxOrder = Post::max('order');
		$add = new Post;

		if (request()->hasFile('image')) {
			$file = request()->file('image');
			$fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
			$file->move('./uploads/posts/', $fileName);
			$add->image = $fileName;
        }

		if(request('status')) $add->status = request('status');
		else $add->status = 0;
		
		$add->title = request('title');
		$add->description = request('description');
		$add->user_id = $this->user->id;
		$add->type = request('type');
		if($maxOrder) $add->order = $maxOrder+1;
		else $add->order = 1;
		$add->save();

		return back()->with(array('success'=>'Saved Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * Change Status.
     *
     * @param  int  $id, int $status
     * @return \Illuminate\Http\Response
     */
	public function status($id, $status)
    {
		if(Post::find($id)->update(array('status'=>$status))) $message = array('success'=>'Saved Successfully');
		else $message = array('fail'=>'Somthing wrong');
		return back()->with($message);
    }
	
	/**
     * Change Order.
     *
     * @param  int  $id, int $status
     * @return \Illuminate\Http\Response
     */
	public function order($order)
    {
		$arr = array();

		$arr = explode('_',$order);
		$post_1 = Post::where('order',$arr[0])->get()->first();
		$post_2 = Post::where('order',$arr[1])->get()->first();
		  
		$post_1->order = $arr[1];
		$post_1->save();
		  
		$post_2->order = $arr[0];
		$post_2->save();
		  
		return back();
    }
}
