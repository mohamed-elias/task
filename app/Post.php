<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'posts';

	protected $fillable = [
        'title', 'description', 'image', 'type', 'order', 'status', 'user_id',
    ];

	public  $timestamps= true;

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function type()
	{
		return $this->belongsTo('App\Type', 'type');
	}
}
