<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	protected $table = 'types';

	protected $fillable = [
        'title',
    ];

	public  $timestamps= true;

	public function post()
	{
		return $this->hasMany('App\Post', 'type', 'id');
	}
}
