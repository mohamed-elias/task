@extends('admin.layouts.app')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Post Page</h2>
            </div>

                        <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<div class="add_part" style="display:block;">
                        <div class="header">
                            <h2>
                                New Post
                            </h2>
                        </div>
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

							@if ($errors->any())
								<div class="alert bg-red alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									@foreach ($errors->all() as $error)
											{{$error}}
											<br>
									@endforeach
								</div>
							@endif

							@if (session('success'))
								<div class="alert bg-green alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									{{session('success')}}
								</div>
							@endif
						
                            <div class="row clearfix">
                                <div class="col-sm-12">
								{!! Form::open(['url' => 'admin/posts/add', 'files' => true]) !!}
									<div class="form-group">
                                        <div class="form-line">
											{{ Form::label('Type:', null, ['for' => 'type']) }}
											<select name="type" class="form-control show-tick">
											@foreach($types as $type)
												<option value="{{$type->id}}">{{$type->title}}</option>
											@endforeach
											</select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
											{{ Form::label('Title:', null, ['for' => 'title']) }}
											{!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' =>'Title']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
											{{ Form::label('Description:', null, ['for' => 'description']) }}
                                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' =>'Description']) !!}
                                        </div>
                                    </div>
									<div class="form-group">
                                        <div class="form-line">
											{{ Form::label('Image:', null, ['for' => 'image']) }}
											{!! Form::file('image') !!}
                                        </div>
                                    </div>
									<div class="form-group">
                                        <div class="form-line">
											<div class="switch">
												<label>OFF{!! Form::checkbox('status', true, true) !!}<span class="lever"></span>ON</label>
											</div>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <div class="form-line">
                                            <button type="submit" class="btn bg-blue btn-circle-lg waves-effect waves-circle waves-float"><i class="material-icons">add</i></button>
                                        </div>
                                    </div>
								{!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</div>
			
                        <div class="body">
						@if ($errors->any())
							<div class="alert bg-red alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								@foreach ($errors->all() as $error)
										{{$error}}
										<br>
								@endforeach
							</div>
						@endif
		
						@if (session('success'))
							<div class="alert bg-green alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								{{session('success')}}
							</div>
						@endif
						@if (!empty($alldata))
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Size</th>
											<th>Settings</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									@foreach ($alldata as $key => $data)
                                        <tr id="row{{$data->id}}">
                                            <td id="key{{$data->id}}">{{$key+1}}</td>
                                            <td id="a{{$data->id}}">{{str_replace(' ','&nbsp;',$data->name)}}</td>
											<td><button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float icon_edit" id="icon{{$data->id}}" onclick="row_edit({{$data->id}});"><i class="material-icons">edit</i></button></td>
                                        </tr>
									@endforeach
                                    </tbody>
                                </table>
                            </div>
						@endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{asset('resources/assets/admin/plugins/jquery/jquery.min.js')}}"></script>
	
	<script>
		function row_edit(id) {
			var id = id;
			var a = $('#a'+id).html();
			$('#a'+id).html('');
			$('#row'+id).append('<form id="form'+id+'" method="POST" action="categories/edit/'+id+'" accept-charset="UTF-8"></form>');
			$('#a'+id).append('<input type="text" class="form-control" id="name'+id+'" name="name" value='+a+'>&nbsp;<button type="submit" class="btn bg-green btn-circle waves-effect waves-circle waves-float" onclick="row_submit('+id+')"><i class="material-icons">done</i></button>');
			$('.icon_edit').attr('disabled','disabled');
		}
		
		function row_submit(id) {
			$('#form'+id).html('<input name="_token" type="hidden" value="'+$("input[name=_token]").val()+'" /><input type="hidden" name="name" value="'+$('#name'+id).val()+'" />');
			$('#form'+id).submit();
		}

	</script>
	
	
@endsection