@extends('admin.layouts.app')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Posts Page</h2>
            </div>

                        <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
						<div class="header">
                            <h2>
                                Posts
                            </h2>
                        </div>					
                        <div class="body">
						
							@if (session('success'))
								<div class="alert bg-green alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									{{session('success')}}
								</div>
							@endif
							@if (session('fail'))
								<div class="alert bg-red alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									{{session('fail')}}
								</div>
							@endif
						
						@if (!empty($alldata))
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
											<th>Added  By</th>
											<th>Type</th>
                                            <th>Title</th>
											<th>Description</th>
											<th>Image</th>
											<th>Order</th>
											<th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									@foreach ($alldata as $key => $data)
                                        <tr>
											<?php $pre=$data->order-1; ?>
											<?php $nxt=$data->order+1; ?>
                                            <td>{{$key+1}}</td>
											<td>{{$data->user()->first()->name}}</td>
											<td>{{$data->type()->first()->title}}</td>
                                            <td>{{$data->title}}</td>
											<td><?php echo htmlspecialchars_decode(stripslashes($data->description)); ?></td>
											<td><img src="{{url('uploads/posts/'.$data->image)}}" width="100px"></td>
											<td>@if($data->order!=1)<a href="{{url('admin/posts/order/'.$data->order.'_'.$pre)}}"><span class="glyphicon glyphicon-arrow-up"></span></a>@endif @if($data->order!=$max)<a href="{{url('admin/posts/order/'.$data->order.'_'.$nxt)}}"><span class="glyphicon glyphicon-arrow-down"></span></a>@endif</td>
											<td><div class="switch"><label>OFF{!! Form::checkbox('status', true, $data->status, ['onchange'=>"handleChange($data->id,this);"]) !!}<span class="lever"></span>ON</label></div></td>
                                        </tr>
									@endforeach
                                    </tbody>
                                </table>
								{!! $alldata->render() !!}
                            </div>
						@endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{asset('resources/assets/admin/plugins/jquery/jquery.min.js')}}"></script>
	
	<script type="text/javascript">
		function handleChange(id,checkbox) {
			if(checkbox.checked == true){
				window.location.href = '{{url("admin/posts/status/")}}'+'/'+id+'/'+1;
			}else{
				window.location.href = '{{url("admin/posts/status")}}'+'/'+id+'/'+0;
			}
		}
	</script>
	
@endsection