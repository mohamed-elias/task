@extends('admin.layouts.app')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Home Page</h2>
            </div>

                        <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Statistics</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="pie_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{asset('resources/assets/admin/plugins/jquery/jquery.min.js')}}"></script>
	
	<script type="text/javascript">
		function handleChange(id,checkbox) {
			if(checkbox.checked == true){
				window.location.href = '{{url("admin/posts/status/")}}'+'/'+id+'/'+1;
			}else{
				window.location.href = '{{url("admin/posts/status")}}'+'/'+id+'/'+0;
			}
		}
	</script>
	<script>
	$(function () {
    new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
});

function getChartJs(type) {
    var config = null;
	if (type === 'pie') {
        config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: ['{{$feat_posts_count}}','{{$none_posts_count}}'],
                    backgroundColor: [
                        //"rgb(233, 30, 99)",
                        "rgb(255, 193, 7)",
                        "rgb(0, 188, 212)",
                        //"rgb(139, 195, 74)"
                    ],
                }],
                labels: [
                    //"Pink",
                    "Featured Posts Count",
                    "Non-Featured Posts Count",
                    //"Light Green"
                ]
            },
            options: {
                responsive: true,
                legend: false
            }
        }
    }
    return config;
}
	</script>
@endsection