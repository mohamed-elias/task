<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Stack - Bootstrap 4 Business Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/bootstrap.min.css')}}" >
    <!-- Icon -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/fonts/line-icons.css')}}">
    <!-- Slicknav -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/slicknav.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/owl.theme.css')}}">
    
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/nivo-lightbox.css')}}">
    <!-- Animate -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/animate.css')}}">
    <!-- Main Style -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/main.css')}}">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{asset('resources/assets/web/assets/css/responsive.css')}}">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a href="{{url('/')}}" class="navbar-brand"><img src="{{url('resources/assets/web/assets/img/logo.png')}}" alt=""></a>       
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="#hero-area">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#non_featured">
                  Non-Featured Posts
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#featured">
                  Featured Posts
                </a>
              </li>
			  @if (Auth::guest())
			  <li class="nav-item">
                <a class="nav-link" href="{{url('login')}}">
                  Login
                </a>
              </li>
			  @else
			  <li class="nav-item">
                <a class="nav-link" href="{{url('admin')}}">
                  Admin
                </a>
              </li>
			  @endif
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
      <div id="hero-area" class="hero-area-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="contents text-center">
                <h2 class="head-title wow fadeInUp">We Discover, Design & Build Digital <br> Presence of Businesses</h2>
                <div class="header-button wow fadeInUp" data-wow-delay="0.3s">
                  <a href="#featured" class="btn btn-common">Explore</a>
                </div>
              </div>
              <div class="img-thumb text-center wow fadeInUp" data-wow-delay="0.6s">
                <img class="img-fluid" src="{{url('resources/assets/web/assets/img/hero-1.png')}}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Hero Area End -->

    </header>
    <!-- Header Area wrapper End -->
	
	<!-- Blog Section -->
    <section id="featured" class="section-padding">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Featured Posts</h2>
        </div>
        <div class="row">

		@foreach($feat_posts as $feat_post_k => $feat_post)
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 blog-item">
            <!-- Blog Item Starts -->
            <div class="blog-item-wrapper wow fadeInLeft" data-wow-delay="0.3s">
              <div class="blog-item-img">
                <a href="single-post.html">
                  <img src="{{url('uploads/posts/'.$feat_post->image)}}" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <h3>
					{{$feat_post->title}}
                </h3>
				<h5>
					{{'Type: '.$feat_post->type()->first()->title}}
                </h5>
				<h5>
					{{'Added By '.$feat_post->user()->first()->name}}
                </h5>
                <p>
					<?php echo htmlspecialchars_decode(stripslashes($feat_post->description)); ?>
                </p>
              </div>
            </div>
            <!-- Blog Item Wrapper Ends-->
          </div>
		  @endforeach

        </div>
      </div>
    </section>
    <!-- Blog Section Ends -->

    <!-- Counter Section Start -->
    <section id="counter" class="section-padding">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="row">
			  <!-- Start counter -->
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="counter-box wow fadeInUp" data-wow-delay="0.8s">
                  <div class="icon-o"><i class="lni-thumbs-up"></i></div>
                  <div class="fact-count">
                    <h3><span class="counter">{{count($none_posts)}}</span></h3>
                    <p>Non-Featured Posts</p>
                  </div>
                </div>
              </div>
              <!-- End counter -->
              <!-- Start counter -->
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="counter-box wow fadeInUp" data-wow-delay="0.4s">
                  <div class="icon-o"><i class="lni-emoji-smile"></i></div>
                  <div class="fact-count">
                    <h3><span class="counter">{{count($feat_posts)}}</span></h3>
                    <p>Featured Posts</p>
                  </div>
                </div>
              </div>
              <!-- End counter -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Counter Section End -->
    
    <!-- Blog Section -->
    <section id="non_featured" class="section-padding">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Non-Featured Posts</h2>
        </div>
        <div class="row">
		
		@foreach($none_posts as $none_post_k => $none_post)
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 blog-item">
            <!-- Blog Item Starts -->
            <div class="blog-item-wrapper wow fadeInLeft" data-wow-delay="0.3s">
              <div class="blog-item-img">
                <a href="single-post.html">
                  <img src="{{url('uploads/posts/'.$none_post->image)}}" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <h3>
					{{$none_post->title}}
                </h3>
				<h5>
					{{'Type: '.$none_post->type()->first()->title}}
                </h5>
				<h5>
					{{'Added By '.$none_post->user()->first()->name}}
                </h5>
                <p>
					<?php echo htmlspecialchars_decode(stripslashes($none_post->description)); ?>
                </p>
              </div>
            </div>
            <!-- Blog Item Wrapper Ends-->
          </div>
		  @endforeach

        </div>
      </div>
    </section>
    <!-- Blog Section Ends -->
    
    <!-- Copyright Section Start -->
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12">
            <div class="footer-logo">
              <img src="{{url('resources/assets/web/assets/img/logo.png')}}" alt="">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12">
            <p class="float-right">Designed and Developed by <a href="http://uideck.com" rel="nofollow">UIdeck</a></p>
          </div> 
        </div>
      </div>
    </div>
    <!-- Copyright Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('resources/assets/web/assets/js/jquery-min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/jquery.mixitup.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/wow.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/jquery.nav.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/scrolling-nav.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/jquery.counterup.min.js')}}"></script>  
    <script src="{{asset('resources/assets/web/assets/js/nivo-lightbox.js')}}"></script>     
    <script src="{{asset('resources/assets/web/assets/js/jquery.magnific-popup.min.js')}}"></script>     
    <script src="{{asset('resources/assets/web/assets/js/waypoints.min.js')}}"></script>   
    <script src="{{asset('resources/assets/web/assets/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/main.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/form-validator.min.js')}}"></script>
    <script src="{{asset('resources/assets/web/assets/js/contact-form-script.min.js')}}"></script>
      
  </body>
</html>
